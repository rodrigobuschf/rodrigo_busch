<?php
	get_header();
?>
	
	<div class="newsletter">
		<div class="container">
			<h2>
				Como ganhar dinheiro diante das variações do Dólar? Um guia estratégico para tempos de incertezas.
			</h2>
			<p>
				Preencha seu email abaixo que te enviaremos o relatório gratuito
			</p>
			<input type="email" name="email" placeholder="seunome@seuprovedor.com.br" /><button>RECEBER</button>
		</div>
		<!-- <div class="bt-meio">
			<i class="fa fa-angle-down" aria-hidden="true"></i>
		</div> -->
	</div>
	<div class="bt-meio">
		<a href="#" id="descer">
			<i class="fa fa-angle-down" aria-hidden="true"></i>
		</a>
	</div>
	<div class="relatorio" id="relatorio">

		<div class="container">
			<?php
				$query_relatorio = new WP_Query( array('post_type' => 'secoes', 'name' => 'por-que-baixar-esse-relatorio') );

				if($query_relatorio->have_posts()):
					$query_relatorio->the_post();
			?>
					<h2>
						<?php
							echo the_title();
						?>
					</h2>
					<?php
						the_content();
					?>
			<?php 
					
				endif;
				wp_reset_query();
				wp_reset_postdata();
			?>
		</div>
	</div>

	<div class="informacoes">
		<div class="container">
			
			<?php
				$query_info = new WP_Query( array('post_type' => 'secoes', 'name' => 'informacoes') );

				if($query_info->have_posts()):
					$query_info->the_post();
			?>
					
					<?php
						the_content();
					?>

			<?php 
					
				endif;
				wp_reset_query();
				wp_reset_postdata();
			?>
			<a href="#" id="subir" class="voltar-ao-topo">
				<i class="fa fa-angle-up" aria-hidden="true"></i> VOLTAR AO TOPO
			</a>

		</div>
	</div>

	<div class="conheca">
		<div class="container">
			<?php
				$query_conheca = new WP_Query( array('post_type' => 'secoes', 'name' => 'conheca-a-empiricus') );

				if($query_conheca->have_posts()):
					$query_conheca->the_post();
			?>
					<h2>
						<?php
							echo the_title();
						?>
					</h2>
					<?php
						the_excerpt();
					?>
					<a href="<?php the_permalink(); ?>">
						SAIBA MAIS
					</a>

			<?php 
					
				endif;
				wp_reset_query();
				wp_reset_postdata();
			?>
		</div>
	</div>
	

<?php
	get_footer();
?>