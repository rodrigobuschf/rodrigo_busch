<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8"/>
	<?php
		if(is_home()){
	?>
		<title><?php bloginfo('name'); ?></title>
		<meta name="title" content="<?php bloginfo('name'); ?>"/>
	<?php
		} else {
	?>
		<title><?php wp_title(); ?></title>
		<meta name="title" content="<?php wp_title(); ?>"/>
	<?php
		}
	?>

	<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	

	<meta name="description" content="<?php bloginfo('description'); ?>" />

	<meta content="palavras Chave" name="keywords">
	<meta content="PT-BR" name="language">
	<meta content="7 days" name="revisit-after">
	<meta content="global" name="distribution">
	<meta content="all" name="audience">
	<meta content="index, follow" name="robots">
	<meta content="<?php echo get_site_url(); ?>" name="url">
	<meta content="" name="classification">
	<meta content="general" name="rating">
	<meta content="Nome da agência" name="author">
	<meta content="Nome da agência" name="creator">
	<meta content="Nome da agência" name="publisher">
	<meta content="Copyright Nome do Cliente" name="copyright">
	<meta content="no" http-equiv="imagetoolbar">
	<meta content="true" name="MSSmartTagsPreventParsing">
	<meta name="robots" content="All"/>
	<link href="humans.txt" rel="author" />

	

	

	<link href="https://fonts.googleapis.com/css?family=Raleway:400,500,600,800" rel="stylesheet">
	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

	<link href="<?php echo get_stylesheet_directory_uri(); ?>/style.css" rel="stylesheet">

	
	<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/jquery-1.9.1.min.js"></script>

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

	<script type="text/javascript">
	    $(document).ready(function() {
	       	$('#subir').click(function(){ 
	          	$('html, body').animate({scrollTop:0}, 'slow');
	      		return false;

	        });

	        //Verifica se a Janela está no topo
		    $(window).scroll(function(){
		        if ($(this).scrollTop() > 100) {
		            $('#subir').fadeIn();
		        } else {
		            $('#subir').fadeOut();
		        }
		    });

	    });


	</script>
	
</head>
<body>
	<header>
		<div class="container"> 
			<div class="logo">
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logo.png" />
			</div>
			<div class="navegacao">
				<?php 
					wp_nav_menu(array('primary'));
				?>
			</div>
		</div>
		
	</header>