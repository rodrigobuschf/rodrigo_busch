	<footer>
		<div class="container">
			<div class="info endereco">
				<h4>Endereço</h4>
				<p>
					<i class="fa fa-map-marker" aria-hidden="true"></i>
					Rua Joaquim Floriano, 913 - 4º andar CEP: 04354-013 - Itaim Bibi. São Paulo/SP
				</p>
			</div>
			<div class="info sociais">
				<h4>Redes sociais</h4>
				<ul>
					<li>
						<a href="#">
							<i class="fa fa-facebook" aria-hidden="true"></i>
						</a>
					</li>
					<li>
						<a href="#">
							<i class="fa fa-twitter" aria-hidden="true"></i>
						</a>
					</li>
					<li>
						<a href="#">
							<i class="fa fa-linkedin" aria-hidden="true"></i>
						</a>
					</li>
					<li>
						<a href="#">
							<i class="fa fa-google-plus" aria-hidden="true"></i>
						</a>
					</li>
					<li>
						<a href="#">
							<i class="fa fa-whatsapp" aria-hidden="true"></i>
						</a>
					</li>
					<li>
						<a href="#">
							<i class="fa fa-envelope-o" aria-hidden="true"></i>
						</a>
					</li>
				</ul>
			</div>
			<div class="info sobre">
				<h4>Sobre nós</h4>
				<p>
					Conheça mais sobre esse e outros assuntos em empiricus.com.br
				</p>
			</div>
			<div class="info informacoes-rodape">
				<p>
					Ao se cadastrar você está concordando com os termos de nossa Política de Privacidade. Você pode cancelar sua inscrição a qualquer momento. Este é um serviço gratuito oferecido pela Empiricus. O cadastro em nossa newsletter diária GRÁTIS te habilita a receber este relatório. Nós NÃO compartilharemos seu endereço de e-mail com ninguém.
				</p>
				<p>
					Copyright © Empiricus Research <?php echo date('Y') ?>
				</p>
			</div>
		</div>
	</footer>
</body>
</html>