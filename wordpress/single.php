<?php
	get_header();
?>

<section class="container pagina-leitura">
	<?php
		if(have_posts()):
			the_post();
	?>
	<div class="col-md-12">
		
		<h1 class="title-padrao">
			<?php echo the_title(); ?>
		</h1>
		<p>
			<?php the_content() ?>
		</p>
		
	</div>
	<div class="col-md-12 col-sm-12 col-xs-12 icon-compatilhamento text-right">
		<div>
			<a href="http://www.facebook.com/sharer.php?u=<?php the_permalink() ?>" class="icons">
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/imagem/iconface.jpg" alt="Compartilhar Facebook" />
			</a>
			<a href="http://twitter.com/home?status=<?php the_permalink() ?>" class="icons">
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/imagem/icontwitter.jpg" alt="Compartilhar Twitter" />
			</a>
			<!-- <a href="#" class="icons">
				<img src="<?php //echo get_stylesheet_directory_uri(); ?>/imagem/icongoogle.jpg" alt="Compartilhar Google+" />
			</a> -->
		</div>
	</div>
	<?php
		endif;
	?>
</section>

<?php
	get_footer();
?>