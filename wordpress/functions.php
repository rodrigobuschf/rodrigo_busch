<?php

if (!function_exists( 'rodrigo_busch_setup' )):

	function rodrigo_busch_setup() {

		
		
		add_theme_support( 'post-thumbnails' );

		
		register_nav_menus( array(
			'primary'   => __( 'Menu Principal', 'rodrigo_busch' )
		) );

		add_theme_support( 'post-formats', array(
			'aside', 'image', 'video', 'audio', 'quote', 'link', 'gallery',
		) );


	}

endif; // twentyfourteen_setup
add_action( 'after_setup_theme', 'rodrigo_busch_setup' );


add_action('init', 'type_post_noticias');

function type_post_noticias() { 
	$labels = array(
		'name' => _x('Seções', 'post type general name'),
		'singular_name' => _x('Seções', 'post type singular name'),
		'add_new' => _x('Adicionar Novo', 'Novo item'),
		'add_new_item' => __('Novo Item'),
		'edit_item' => __('Editar Item'),
		'new_item' => __('Novo Item'),
		'view_item' => __('Ver Item'),
		'search_items' => __('Procurar Itens'),
		'not_found' =>  __('Nenhum registro encontrado'),
		'not_found_in_trash' => __('Nenhum registro encontrado na lixeira'),
		'parent_item_colon' => '',
		'menu_name' => 'Cadastro das Seções'
	);

	$args = array(
		'labels' => $labels,
		'public' => true,
		'public_queryable' => true,
		'show_ui' => true,			
		'query_var' => true,
		'rewrite' => true,
		'capability_type' => 'post',
		'has_archive' => true,
		'hierarchical' => false,
		'menu_position' => null,
		'register_meta_box_cb' => 'secoes_meta_box',		
		'supports' => array('title','editor','thumbnail','comments', 'excerpt', 'custom-fields', 'revisions', 'trackbacks')
      );
 
register_post_type( 'secoes' , $args );
flush_rewrite_rules();
}
