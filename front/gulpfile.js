// VARS
var gulp = require( 'gulp' ),
	less = require('gulp-less'),
	prefix = require('gulp-autoprefixer'),
	minifycss = require('gulp-clean-css'),
	livereload = require('gulp-livereload'),
	connect = require('gulp-connect'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    browserSync = require('browser-sync').create();

// HTML
gulp.task('html', function () {
  //gulp.src(['*.shtml', 'inc/*.html'])
  gulp.src(['*.html', 'inc/*.html'])
    .pipe(browserSync.stream());
});

// STYLES
gulp.task('styles', function() {
	gulp.src('css/less/*.less')
	.pipe(less())
	.pipe(prefix('last 3 version'))
	.pipe(minifycss())
	.pipe(gulp.dest('css'))
	.pipe(browserSync.stream())
});

// SCRIPTS 

//INSTITUCIONAL 
gulp.task('scripts', function() {
  return gulp.src(['js/assets/jquery-1.9.1.min.js', 'js/assets/controller.js'])
    .pipe(concat('main.min.js'))
    .pipe(uglify())
    .pipe(gulp.dest('js'))
    .pipe(browserSync.stream());
});

// WATCH
gulp.task('watch', function() {
	//gulp.watch(['*.shtml', 'inc/*.html'], ['html']);
    gulp.watch(['*.html', 'inc/*.html'], ['html']);
    gulp.watch(['js/assets/*.js'], ['scripts']);
    gulp.watch([
    	'css/less/*.less'
    ], ['styles']);
});

// Static server
gulp.task('browser-sync', function() {
    browserSync.init({
            proxy: "localhost"
        }
    );
});

gulp.task('default', ['styles', 'watch', 'browser-sync', 'scripts']);